var express = require('express')
var app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const fileUpload = require("express-fileupload");
var path= require('path')

const router = require('./src/routes')

var corsOptions = {
    origin: "*"
}


app.use(
  fileUpload()
);

//bodyparser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//static akses
app.use('/public', express.static('public'))
app.use('/image', express.static('image'))
//cors
app.use(cors(corsOptions))
//setting express
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//route
app.use("/mahasiswa", router)

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})