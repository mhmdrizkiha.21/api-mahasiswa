# api-mahasiswa



# Route
1. GET '/mahasiswa'
2. GET '/mahasiswa/search?name=value'
3. GET '/mahasiswa/:nim'
4. POST '/mahasiswa'
5. PUT '/mahasiswa/:nim'
6. DELETE '/mahasiswa/:nim'

SERVER : localhost:3000

# Installation
1. clone repository
2. cd api-mahasiswa
3. npm install
4. npm run start (menjalankan server)

# Struktur folder and file
1. server.js --> script app
2. /src/routes.js --> routes
3. /src/model --> folder berisi model dan db
4. /src/controller --> folder berisi controller
5. /public/image/ --> folder berisi foto upload ketika create data

